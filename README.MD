# Reproduce

## Deploy

```
# Init
# Create your service account key with Kubernetes Engine Service Agent role. Save it as sa.json inside workspace
export GOOGLE_APPLICATION_CREDENTIALS="./sa.json"
gcloud auth activate-service-account --key-file=$GOOGLE_APPLICATION_CREDENTIALS --project=<your_gcp_project>
# Create an autopilot cluster in region europe-west-1. Name it ac-1
gcloud container clusters get-credentials ac-1 --region europe-west1
```

## Test actions

1. Deploy a first time the workload
2. Get load balancer IP from kubectl get ingress. Export it as an environment variable : export NGINX_PUBLIC_IP=<your_ip>
3. Attendez quelques minutes puis exécuter la création de la règle firewall pour votre cluster contenue dans les logs events de l'ingress : kubectl describe ingress
3. Launch monitoring windows (see below)
4. Change version labels inside deployment and create new deployment (cf helm upgrade)
5. Observe errors 502

### Operations

- (new window) Deploy release : ```kubectl apply -f ./k8s```
- (left window) Monitor HTTP : ```while [ true ]; do echo "$(date) - $(curl --http1.1 -sI http://$NGINX_PUBLIC_IP | head -1)"; sleep 0.5; done```
- (middle window) Monitor HTTP : ```watch -n0.5 kubectl get pods -n kube-system -l component=kube-proxy```
- (right window) Monitor pods and nodes : ```watch -n0.5 kubectl get pods,nodes,endpoints```